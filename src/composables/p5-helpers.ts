import p5 from "p5";

// TODO: proper typing

type SketchDataClosure = (setup_data: object, draw_data: object) => ((p: p5) => (p: p5) => p5);
type SketchClosure = (p: p5) => (p: p5) => p5;

export function useP5Helpers() {
  async function p5_bind_sketch(sketch: SketchClosure, elementId : string) : Promise<p5> {
    try {
      const canvas = document.getElementById(elementId);
      if (!canvas) {
        throw new Error(`Cannot bind P5 sketch with HTMLelement #${elementId}.`);
      }
      return await new p5(sketch, canvas);
    } catch (e) {
      return Promise.reject(new Error(`Couldn't load sketch in element #${elementId}: ` + e));
    }
  }

  function p5_bind_data(sketch_closure : SketchDataClosure, setup_data : Object, draw_data: Object) : any {
    return sketch_closure(setup_data, draw_data);
  }

  function p5_get_canvas_image(elementId : string) : string {
    try {
      const canvas = document.getElementById(elementId) as HTMLCanvasElement;
      if (!canvas) {
        throw new Error(`Cannot find canvas in HTMLelement #${elementId}.`);
      }
      return canvas.toDataURL("image/jpeg").split(';base64,')[1];
    } catch (e) {
      throw new Error("Cannot get canvas image: " + e);
    }
  }

  return { p5_bind_sketch, p5_bind_data, p5_get_canvas_image };
}

export type Vec2D = {
    x: number;
    y: number;
};

// Inheritance example using &
/*type Vec3D = Vec2D & {
    z: number;
}*/
import p5 from 'p5'

import * as noises from './common/noise.js';

export interface SketchData {
  canvasWidth: number;
  canvasHeight: number;
  noiseScale: number;
}

interface Agent {
  pos: p5.Vector;
}

const s_test = (data: SketchData, ) => {
  const sketch = (p: p5) => {
    let agentsNum = 1000;
    let noiseImg : p5.Graphics;
    const agents : Agent[] = [];
    
    p.setup = function () {
      console.log(`Setup sketch: ${JSON.stringify(data)}`);
      p.createCanvas(data.canvasWidth, data.canvasHeight);
      p.colorMode(p.HSB, 360, 100, 100, 1);
      p.noiseDetail(1, 0);

      noiseImg = p.createGraphics(data.canvasWidth, data.canvasHeight);
      p.generateNoiseImg(p, noiseImg);
      //noiseImg.textSize(200);
      //noiseImg.text("hej!", 100, 200);

      for (let i = 0; i < agentsNum; i++) {
        const a : Agent = { pos: p.createVector(p.random(p.width), p.random(p.height)) }; // is width-height the same?
        agents.push(a);
      }
    };

    p.draw = function () {
      p.background(0,100,200, 0.1);
      p.image(noiseImg, 0, 0);

      // agents
      p.strokeWeight(4);
      p.stroke(200);
      /* for (const a of agents) {
        a.pos.add(curl(a.pos.x/data.noiseScale, a.pos.y/data.noiseScale));
        p.point(a.pos);
      } */
    };

    function curl(x: number, y: number) {
      const EPS = 0.001;
      let n1 = p.noise(x + EPS , y);
      noiseImg.loadPixels();
      let d = p.pixelDensity();
      let widthd = data.canvasWidth*d;
      n1 = noiseImg.pixels[(x+y*widthd)*4];
      let n2 = p.noise(x - EPS , y);
      n2 = noiseImg.pixels[(x+widthd+(y+1)*widthd)*4];
      const cx = (n1 - n2) / (2 * EPS);
      n1 = p.noise(x, y + EPS);
      n2 = p.noise(x, y - EPS);
      const cy = (n1 - n2) / (2 * EPS);
      return p.createVector(cx, cy);
    }

    p.regenerate = function () {
      console.log("regenerate() called")
      p.generateNoiseImg(noiseImg);
    }

    p.generateNoiseImg = function (buf: p5.Graphics) {
      console.log("generateNoiseImg() called");
      buf.loadPixels();

      let noiseScale = data.noiseScale;
      let widthd = data.canvasWidth*p.pixelDensity();
      let heightd = data.canvasHeight*p.pixelDensity();

      for (let i = 0; i < widthd; i++) {
        for (let j = 0; j < heightd; j++) {
          const x = i / p.pixelDensity();
          const y = j / p.pixelDensity();
          const bright = p.pow(p.noise(x/noiseScale, y/noiseScale)-0.3, 1/2.0) * 400;
          // p.map(noises.simplex2(x * noiseScale, y * noiseScale), 0, 1, 0, 360);
          buf.pixels[(i+j*widthd)*4] = bright;
          buf.pixels[(i+j*widthd)*4+1] = bright;
          buf.pixels[(i+j*widthd)*4+2] = bright;
          buf.pixels[(i+j*widthd)*4+3] = 255;
        }
      }
      buf.updatePixels();
    }
  };
  return sketch;
};

export default s_test;
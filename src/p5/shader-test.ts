import p5 from "p5";

export interface DataSetup {
  canvasWidth: number;
  canvasHeight: number;
}

export interface DataDraw {
}

interface p5_Extended extends p5 {
  reset: () => void;
  clear: () => any; // is wrongly typed in p5
}

const sketch_wrapped = (data_setup: DataSetup, data_draw: DataDraw) => {
  const sketch = async (p: p5_Extended) => {

    let theShader : p5.Shader;
    let shaderTexture : p5.Graphics;
    let buf_test : p5.Graphics;

    p.preload = function() {
      theShader = p.loadShader('shaders/recursive-noise/shader.vert', 'shaders/recursive-noise/shader.frag');
    }

    p.setup = function () {
      p.colorMode(p.HSB);
      p.createCanvas(data_setup.canvasWidth, data_setup.canvasHeight, p.WEBGL);
      p.noStroke();
      p.pixelDensity(1); // for retina screens

      shaderTexture = p.createGraphics(p.width, p.height, p.WEBGL);
      shaderTexture.noStroke();

      buf_test = p.createGraphics(p.width, p.height);
      p.translate(-p.width/2, -p.height/2);
    };

    p.reset = function () {
      console.log("Sketch reset");
      p.clear();
    };

    p.draw = function () {
      p.clear();

      buf_test.fill(150,100,90);
      buf_test.rect(0,0,400,500);
      p.image(buf_test, 0, 0);
      
      theShader.setUniform("iResolution", [p.width, p.height]);
      theShader.setUniform("iFrame", p.frameCount);
      theShader.setUniform("iMouse", [p.mouseX, p.map(p.mouseY, 0, p.height, p.height, 0)]);
      
      shaderTexture.shader(theShader);
      shaderTexture.rect(0, 0, p.width, p.height);
      p.image(shaderTexture,0,0);
      
      p.fill(0,100,100);
      p.rect(-40,-400,100,400);
      p.texture(shaderTexture);
      p.rect(0,0,200,200);
    };

  };
  return sketch;
};

export default sketch_wrapped;

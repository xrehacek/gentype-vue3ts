import p5 from "p5";

export interface DataSetup {
  canvasWidth: number;
  canvasHeight: number;
}

export interface DataDraw {
}

interface p5_Extended extends p5 {
  reset: () => void;
  clear: () => any; // is wrongly typed in p5
}

const sketch_wrapped = (data_setup: DataSetup, data_draw: DataDraw) => {
  const sketch = async (p: p5_Extended) => {

    p.setup = function () {
      p.colorMode(p.HSB, 360, 100, 100, 1);
      p.createCanvas(data_setup.canvasWidth, data_setup.canvasHeight);
    };

    p.reset = async function () {
      console.log("Sketch reset");
      p.clear();
    };

    p.draw = function () {
      p.clear();

    };

  };
  return sketch;
};

export default sketch_wrapped;

import p5 from 'p5'

const s_test = ( data: any ) => {
  const sketch = ( p: p5 ) => {
    p.setup = function() {
      p.createCanvas(data.canvasWidth, data.canvasHeight);
      p.colorMode(p.HSB, 360, 100, 100, 1);

      p.fill(220,100,100);
      p.circle(100, 100, 30);
      //console.log("Circle isze: "+data.circle_size);
    };
  
    p.draw = function() {
      p.background(255);
      p.circle(50, 50, data.circle_size*p.sin(p.millis()*data.speed));
      console.log("p5 current speed: "+data.speed);
    };
  };
  return sketch;
};

export default s_test;